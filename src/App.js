import React, { Component } from 'react';
import axios from 'axios';
import './App.css';

export default class App extends Component {
  state = {
    hobbies: [],
    hobby: "",
    email: "",
    emailErr: "",
    phone: "",
    phoneErr: "",
    pwd: "",
    repwd: "",
    pwdErr: "",
    isLoading: false,
    loggedIn: false,
    register: false
  }

  handleLogin(){
    if (this.state.phoneErr === "" && this.state.phone.length > 10){
      this.setState({ isLoading: true })
      axios.post('https://hoblogserver.herokuapp.com/login', {
        phone: this.state.phone,
        password: this.state.pwd
      })
      .then((res) => {
        let data = res.data;
        if (data === "Incorrect") this.setState({ pwdErr: "Incorrect Username or Password" })
        else {
          for (let i = 0; i < data.length; i++){
            let hobbies = this.state.hobbies;
            hobbies.push(data[i].hobby);
            this.setState({ hobbies })
          }
          this.setState({ loggedIn: true })
        }
        this.setState({ isLoading: false })
      })
      .catch(() => {
        alert("Something went wrong. Please check your internet connection and try again")
        this.setState({ isLoading: false })
        this.setState({ loggedIn: true })
      })
    } else this.setState({ phoneErr: "Please enter a correct number" })
  }

  handleRegister(){
    if (this.state.phoneErr === "" && this.state.phone.length > 10 && this.state.pwdErr === "" && this.state.pwd.length > 1){
      this.setState({ isLoading: true })
      axios.post('https://hoblogserver.herokuapp.com/register', {
        email: this.state.email,
        phone: this.state.phone,
        password: this.state.pwd
      })
      .then((res) => {
        if (res.data === "Done"){
          this.setState({ isLoading: false })
          this.setState({ loggedIn: true })
          this.setState({ register: false })
        } else alert("Something went wrong. Please try again")
      })
      .catch(() => alert("Something went wrong. Please check your internet connection and try again"))
    } else this.setState({ pwdErr: "Please fill all fields correctly" });
  }

  handlePhone(e){
    let phone = e.target.value;
    if (phone.length < 11) this.setState({ phoneErr: "Please enter a correct number" })
    else {
      if (phone.startsWith("0") && phone.length === 11) {
        this.setState({ phoneErr: "" });
        phone = phone.split("");
        phone[0] = "+234";
        phone = phone.join("");
      } else if (phone.startsWith("+") && phone.length === 14){
        this.setState({ phoneErr: "" })
      } else this.setState({ phoneErr: "Please enter a correct number" })
    }

    this.setState({ phone })
  }

  handleEmail(e){
    let email = e.target.value;

    if (!email.includes('@') || !email.includes('.')) this.setState({ emailErr: "Please enter a correct email" })
    else this.setState({ emailErr: "" })

    this.setState({ email })
  }

  addHobby(){
    let hobbies = this.state.hobbies;
    let hobby = this.state.hobby;
    if (hobby.length > 0){
      hobbies.push(hobby);
      this.setState({ hobbies })
      this.setState({ input: "" })
      this.hobbyInput.focus();

      axios.post('https://hoblogserver.herokuapp.com/add', {
        hobby: hobby,
        phone: this.state.phone
      })
    }
  }

  render(){
    return (
      <div className="wrapper">
        <div className={this.state.isLoading ? "loading" : "loading hide"}></div>
        <div className={!this.state.loggedIn && !this.state.register ? "auth" : "auth hide"}>
          <h1>hobLog</h1>
          <div>
            <input placeholder="Phone Number" type="email" value={this.state.phone} onChange={this.handlePhone.bind(this)}/>
            <span>{this.state.phoneErr}</span>
          </div>
          <div>
            <input placeholder="Password" type="password" value={this.state.pwd} onChange={(e) => this.setState({ pwd: e.target.value })}/>
            <span>{this.state.pwdErr}</span>
          </div>
          <a href="#!" onClick={(e) => {
            e.preventDefault();
            this.setState({ register: true })
          }}>Don't have an account? Click here to register</a><br/>
          <button onClick={this.handleLogin.bind(this)}>Login</button>
        </div>
        <div className={!this.state.loggedIn && this.state.register ? "auth" : "auth hide"}>
          <h1>Register</h1>
          <div>
            <input placeholder="Email Address" type="email" value={this.state.email} onChange={this.handleEmail.bind(this)}/>
            <span>{this.state.emailErr}</span>
          </div>
          <div>
            <input placeholder="Phone Number" type="tel" value={this.state.phone} onChange={this.handlePhone.bind(this)}/>
            <span>{this.state.phoneErr}</span>
          </div>
          <div>
            <input placeholder="Password" type="password" value={this.state.pwd} onChange={(e) => this.setState({ pwd: e.target.value })}/>
          </div>
          <div>
            <input placeholder="Retype Password" type="password" value={this.state.repwd} onChange={(e) => {
              this.setState({ repwd: e.target.value })
              if (this.state.pwd === e.target.value) this.setState({ pwdErr: "" })
              else this.setState({ pwdErr: "Passwords don't match" })
            }}/>
            <span>{this.state.pwdErr}</span>
          </div>
          <button onClick={this.handleRegister.bind(this)}>Register</button>
        </div>
        <div className={this.state.loggedIn ? "content" : "content hide"}>
          <h1>Your hobLog</h1>
          {this.state.hobbies.map((hobby, i) => {
            return (<p key={i}>{hobby}</p>)
          })}
          <div className="input">
            <input type="text" ref={(input) => { this.hobbyInput = input; }} value={this.state.hobby} onChange={(e) => this.setState({ hobby: e.target.value })}/>
            <button onClick={this.addHobby.bind(this)}>Add</button>
          </div>
        </div>        
      </div>
    )
  }
}